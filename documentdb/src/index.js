import { typeDefs } from "./graphql-schema";
import { ApolloServer } from "apollo-server-express";
import express from "express";
import { MongoClient } from "mongodb"
import dotenv from "dotenv";
import neo4j from 'neo4j-driver';

// set environment variables from ../.env
dotenv.config();

const {
  MONGO_URL,
  GRAPHQL_LISTEN_PORT,
  NEO4J_URI
} = process.env;
const path = "/graphql";


const app = express();

(async () => {
  const client = await MongoClient.connect(MONGO_URL);
  const db = client.db();
  const driver = neo4j.driver(NEO4J_URI);


  const resolvers = {
    Query: {
      // getCandidates: async () => await candidates.find().toArray(),
      // getCandidate: async (_, params) => await candidates.findOne(params),{id:$id}) 
      //  (o:Opportunity{id:$id}) 
      getMatches: async (_, params) => {
        console.log('----------------');
        const graphStart = process.hrtime()
        const graph = await driver.session().run("MATCH (o:Opportunity{id:$id}) OPTIONAL MATCH (o)-[reqSkill:REQUIRES_SKILL]->(sr:Skill) WITH o, collect(sr) AS requiredSkillList WITH o, requiredSkillList, HEAD(requiredSkillList) as skill1 MATCH (c:Candidate) WHERE ( o.experienceRangeMin <= c.experience <= o.experienceRangeMax ) AND ( size(requiredSkillList) = 0 OR ( (c)-[:HAS_EXPERIENCE]->(skill1) AND ALL(skill IN requiredSkillList WHERE (c)-[:HAS_EXPERIENCE]->(skill)) ) ) OPTIONAL MATCH (o)-[:HAS_NATIONALITY]->(nat:Nationality) WITH o, c, collect(nat) as oppNationalities WHERE size(oppNationalities) = 0 OR ANY(nationality IN oppNationalities WHERE (c)-[:HAS_NATIONALITY]->(nationality)) OPTIONAL MATCH (o)-[:HAS_PROFESSION]->(p:Profession) WITH o, c, collect(p) as oppProfessions WHERE size(oppProfessions) = 0 OR ANY(prof IN oppProfessions WHERE (c)-[:HAS_PROFESSION]->(prof)) OPTIONAL MATCH (o)-[:REQUIRES_STUDIES_AT]->(u:University) WITH o, c, collect(u) as oppUniversities WHERE size(oppUniversities) = 0 OR ANY(uni IN oppUniversities WHERE (c)-[:STUDIED_AT]->(uni)) OPTIONAL MATCH (c)-[:HAS_EXPERIENCE]->(optskill:Skill)<-[:OPTIONALLY_REQUIRES_SKILL]-(o) WITH c, o, COLLECT(optskill.id) AS optionalCount, apoc.date.convert(c.birthDate, 'ms', 'd')/365 AS yearsOld WHERE o.ageRangeMin = -1 OR o.ageRangeMin <= yearsOld <=o.ageRangeMax OPTIONAL MATCH (cskill:Skill)<-[:HAS_EXPERIENCE]-(c)-[:HAS_PROFESSION]->(cprof:Profession) WITH c, optionalCount, collect(cskill.id) as candidateSkills, collect(cprof.id) AS candidateProfessions RETURN c.id AS candidate_id  ORDER BY size(optionalCount) DESC", params)
        const graphEnd = process.hrtime(graphStart);
        console.log('Graph execution time: %ds %dms', graphEnd[0], graphEnd[1] / 1000000)
        const mappingStart = process.hrtime();
        const ids = graph.records.map(r => r.get('candidate_id'));
        const mappingEnd = process.hrtime(mappingStart);
        console.log('Mapping execution time: %ds %dms', mappingEnd[0], mappingEnd[1] / 1000000)

        if (!ids) return [];
        // pagino a mano el array que devuelve neo, hay que ver si existe el equivalente al skip y limit
        const { limit = 10, skip = 0 } = params;
        const page = ids.slice(skip, limit);

        console.log(`\tGraph len ${page.length}; id[0]: ${page[0]}, id[last]: ${page[page.length - 1]}`);

        const mongoStart = process.hrtime();
        const opportunities = db.collection('opportunities')

        const { id } = params;
        const { locationsGeometry } = await opportunities.findOne({ id }, { 'locationsGeometry': 1, '_id': 0 })

        const geoQuery = {
          id: { '$in': page }
        }

        if (locationsGeometry) {
          geoQuery['address.location'] =
            {
              '$geoWithin': {
                '$geometry': locationsGeometry
              }
            }
        }
        const candidates = db.collection('candidates');
        const mongoEnd = process.hrtime(mongoStart);
        console.log('Mongodb execution time: %ds %dms', mongoEnd[0], mongoEnd[1] / 1000000)

        const toArray = process.hrtime();
        // saque el .skip(skip).limit(limit) porque lo recorto con el slice de la respuesta de neo
        const geoCandidates = await candidates.find(geoQuery).toArray();
        const toArrayEnd = process.hrtime(toArray);
        console.log('toArray execution time: %ds %dms', toArrayEnd[0], toArrayEnd[1] / 1000000)

        const orderStart = process.hrtime();
        const ordered = [];
        // el .map del array tarda al menos 3x mas
        for (let idx = 0; idx < page.length; idx++) {
          ordered.push(geoCandidates[idx]);
        }
        const orderEnd = process.hrtime(orderStart);
        console.log('order execution time: %ds %dms', orderEnd[0], orderEnd[1] / 1000000)
        const result = ordered.filter(i=>i!==undefined);
        console.log(`\tresult len ${result.length}; id[0]: ${result[0].id}, id[last]: ${result[result.length - 1].id}`);
        const totalTime = process.hrtime(graphStart);
        console.log('TOTAL TIME: %ds %dms', totalTime[0], totalTime[1] / 1000000)

        return result;
      }
    }
  }

  const server = new ApolloServer({
    introspection: true,
    typeDefs,
    resolvers,
    formatError: error => {
      return error
    },
    context: ({ req, res }) => {
      return {
        req, res
      }
    }
  })
  server.applyMiddleware({ app, path })


  app.listen(GRAPHQL_LISTEN_PORT, () => {
    console.log(`app is listening to port ${GRAPHQL_LISTEN_PORT}`);
  })
})()